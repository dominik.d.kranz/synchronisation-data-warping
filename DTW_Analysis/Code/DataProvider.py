import pandas


class DataProvider:
    MOCK_UP = False
    @property
    def speciesA(self):
        return self.__speciesA
    @speciesA.setter
    def speciesA(self, value):
        self.__speciesA = value
    @property
    def speciesB(self):
        return self.__speciesB
    @speciesB.setter
    def speciesB(self, value):
        self.__speciesB = value
    @property
    def data(self):
        return self.__data
    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def timeSeriesA(self):
        return self.__timeSeriesA
    @timeSeriesA.setter
    def timeSeriesA(self, value):
        self.__timeSeriesA = value

    @property
    def timeSeriesB(self):
        return self.__timeSeriesB
    @timeSeriesB.setter
    def timeSeriesB(self, value):
        self.__timeSeriesB = value

    @property
    def timeScale(self):
        return self.__timeScale
    @timeScale.setter
    def timeScale(self, value):
        self.__timeScale = value

    @property
    def newTimeScale(self):
        return self.__newTimeScale
    @newTimeScale.setter
    def newTimeScale(self, value):
        self.__newTimeScale = value

    @property
    def path(self):
        return self.__path
    @path.setter
    def path(self, value):
        self.__path = value

    @property
    def names(self):
        return self.__names
    @names.setter
    def names(self, value):
        self.__names = value

    @property
    def filepath(self):
        return self.__filepath
    @filepath.setter
    def filepath(self, value):
        self.__filepath = value


    def __init__(self):
        self.filepath = 'dataarr.csv'
        self.experimental_data = []
        self.__data = []
        self.__speciesA = []
        self.__speciesB = []
        self.__names = []
        self.__path = []
        self.__timeSeriesA = []
        self.__timeSeriesB = []
        self.__timeScale =[]
        self.__newTimeScale = []
        self.__newTimeSeriesB = []
        if self.MOCK_UP:
            self.__names = ["Time", "Series A", "Series B"]
            return
    def LoadExperimentalDataBlock(self, filepath='dataarr.csv'):
        exp_data = pandas.read_csv(filepath, sep=';')
        self.data = exp_data
        self.speciesA = exp_data.iloc[:, 1:5]
        self.speciesB = exp_data.iloc[:, 5:]
        print(self.speciesA)
        print(self.speciesB)
        self.timeScale = exp_data.iloc[:, 0]
        #print(exp_data.head())
        #print(exp_data.columns.values)
    def LoadRegressionData(self, filepath='regressiondata.csv'):
        data = pandas.read_csv(filepath, sep=';')
        return data
    def LoadExperimentalData(self, filepath='data.csv'):
        exp_data= pandas.read_csv(filepath, sep=';')
        self.data = exp_data
        self.timeScale = exp_data.iloc[:,0]
        self.speciesA = exp_data.iloc[:,1]
        self.speciesB = exp_data.iloc[:,2]
    def LoadSynchroData(self, filepath='synchro_data.csv'):
        data = pandas.read_csv(filepath, sep=';')
        self.data = data
        self.timeScale = exp_data.iloc[:,0]

    def GetSpeciesForPeakAdjustment(self):
        ClnA = self.data.iloc[:,1]
        ClbA = self.data.iloc[:,2]

        ClnB = self.data.iloc[:,5]
        ClbB = self.data.iloc[:,6]
        return([ClnA, ClbA, ClnB, ClbB])

    def CutDataForPeaks(self, firstPeakA, firstPeakB):
        difference = firstPeakA - firstPeakB
        if difference > 0: #move A to the left
            self.__speciesA = self.__speciesA.iloc[difference:]
        if difference < 0: #move B to the left
            self.__speciesB = self.__speciesB.iloc[abs(difference):]

    def SaveRegression(self, params, path='regression_params.csv', sep=';'):
        params.to_csv(index=False, path_or_buf=path, sep=sep)


if __name__ == '__main__':
    dp = DataProvider()
    dp.LoadExperimentalDataBlock()
    dp.GetSpeciesForPeakAdjustment()