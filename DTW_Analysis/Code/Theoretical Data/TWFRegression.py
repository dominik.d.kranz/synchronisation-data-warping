from pandas import DataFrame
import pandas
import numpy
import matplotlib.pyplot as pyplot
from pathlib import Path

def f(xd, yd, synchro_params, true_params):
    return yd - (synchronised_model(synchro_params, xd) - true_model(true_params, xd))

def synchronised_model(params, t): #gaussian
    a1, b1, c1, a2, b2, c2, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + h

def true_model(params, x): #my func
    a1, b1, c1, a2, b2, c2, n = params
    ret = []
    for i in x:
        if i < b1-n*numpy.sqrt(c1):
            ret.append(0)
        else:
           ret.append(a1/(numpy.sqrt(c1)*n)**n * (i-b1+n*numpy.sqrt(c1))**n * numpy.exp(((-i+b1)/numpy.sqrt(c1))))
       
    index = 0
    for i in x:
        if i < b2-n*numpy.sqrt(c2):
            ret[index] += 0
        else:
            ret[index] += a2/(numpy.sqrt(c2)*n)**n * (i-b2+n*numpy.sqrt(c2))**n * numpy.exp(((-i+b2)/numpy.sqrt(c2)))
        index += 1
    return ret

path = Path(__file__).parent
#              a1,  b1, c1, a2,  b2,  c2, n
params_true = [140, 25, 20, 100, 100, 30, 4]
params_synchro = pandas.read_csv(str(path) + r'\regression_params.csv', sep=';').iloc[0].values.tolist()
print(params_synchro)
data_points = pandas.read_csv(str(path) + r'\mock_up.csv', sep=';')
x = data_points.iloc[:, 0]
y = data_points.iloc[:, 1]
warped_data = f(x, y, params_synchro, params_true)
pyplot.plot(x, warped_data, color='green', marker='x', linestyle='')
#pyplot.plot(x, y, color='red', marker='x', linestyle='')
cont_time = numpy.arange(0, 180, 0.1)
true_trend = true_model(params_true, cont_time)
#goodness of fit for true trend <-> warped data for comparisons sake:
#goodness of fit (R^2):
regression_points = true_model(params_true, x) #values of the 'fitted' function at the given points
mean_warped_data = numpy.mean(warped_data) #the mean value of the "experimental" data
residuals = warped_data - regression_points
r_squared = 1.0 - (numpy.sum(residuals ** 2) / numpy.sum((warped_data - mean_warped_data)**2))

pyplot.plot(cont_time, true_trend)
#pyplot.plot(cont_time, synchronised_model(params_synchro, cont_time))
pyplot.legend(['Warped data points', 'True trend, R^2 = ' + str(round(r_squared, 4))])
pyplot.xlabel('Time t [min]')
pyplot.ylabel('Data [AU]')
pyplot.show()

