import numpy
import matplotlib.pyplot as pyplot
from pandas import DataFrame
from pathlib import Path

n = 4

a1 = 100
x0 = 30
c1= 30

a2 = 75
x1 = 120
c2 = 45

#noise parameters:
mu = 0
sigma = 15

def true_func(x):
    ret = []
    for i in x:
        if i < x0-n*numpy.sqrt(c1):
            ret.append(0)
        else:
           ret.append(a1/(numpy.sqrt(c1)*n)**n * (i-x0+n*numpy.sqrt(c1))**n * numpy.exp(((-i+x0)/numpy.sqrt(c1))))
       
    index = 0
    for i in x:
        if i < x1-n*numpy.sqrt(c2):
            ret[index] += 0 
        else:
            ret[index] += a2/(numpy.sqrt(c2)*n)**n * (i-x1+n*numpy.sqrt(c2))**n * numpy.exp(((-i+x1)/numpy.sqrt(c2)))
        index += 1    
    return ret

def add_noise(x):
    noise = numpy.random.normal(mu, sigma, len(x))
    return (x+noise)

timeScale = numpy.arange(0, 180, 10)
timeScale_cont = numpy.arange(0, 180, 0.1)
Y_data = add_noise(true_func(timeScale))
Y_data_cont = true_func(timeScale_cont)

#save data
df = DataFrame()
df['Time'] = timeScale
df['Protein Count'] = Y_data

savepath = str(Path(__file__).parent) + r'\mock_up.csv'
df.to_csv(savepath, sep=';', index = False)

pyplot.plot(timeScale, Y_data, marker='x', linestyle='')
pyplot.plot(timeScale_cont, Y_data_cont)
pyplot.xlabel("Time t [min]")
pyplot.ylabel("Data [AU]")
pyplot.legend(["Theoretical data"])
pyplot.show()




