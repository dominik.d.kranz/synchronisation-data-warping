from pandas import DataFrame
from matplotlib import pyplot
import numpy

savepath='fitting\\dirty_data\\dirty_series5'

a1 = 800000
a2 =  350000
b1 = 60
b2 = 140
c1 = 15
c2 = 30
h = 0
t_start = 0
t_stop = 180
t_points = 19
noise_mean = 30000
noise_sd = 200000
def two_peak_func(t):
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + h

def make_dirty(model):
    return [i + noise_mean * numpy.random.normal(0, 1.0) for i in model]

t = numpy.linspace(t_start, t_stop, t_points)
model = two_peak_func(t)
model = make_dirty(model)

df = DataFrame()
df['Time'] = t
df['Protein Count'] = model
df.to_csv(savepath+'.csv', sep=';', index=False)

pyplot.plot(t, model, 'o', label='Experimental Data')
pyplot.xlabel('Time t')
pyplot.ylabel('Protein Count P')
pyplot.legend()
pyplot.savefig(savepath + '.png')
