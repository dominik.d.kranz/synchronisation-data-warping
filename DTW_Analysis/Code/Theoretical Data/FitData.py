from pandas import DataFrame
import pandas
import scipy.optimize
import numpy
import matplotlib.pyplot as pyplot
from pathlib import Path
#pyplot.style.use('seaborn-pastel')

t_start = 0
t_stop = 180
t_num = 18 #number of data points
error_perc = numpy.linspace(0.05, 0.10, t_num) #weighing of data points

def model_func_2_peaks(params, t):
    a1, b1, c1, a2, b2, c2, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + h

#def residue_func(params, t, y):
#    return numpy.multiply(numpy.divide(1, error_perc), model_func_2_peaks(params, t) - y)
def residue_func(params, t, y):
    return model_func_2_peaks(params, t) - y


#load data
path = str(Path(__file__).parent)
df = pandas.read_csv(path + r'\mock_up.csv', sep=';')
x = df.iloc[:,0]
y = df.iloc[:,1]

params0 = [100.0, 30.0, 10.0, 100.0, 125,  10, 0]
res_lsq = scipy.optimize.least_squares(residue_func, params0, args=(x, y), method='lm')

regression_params = res_lsq.x
#save regression params
dic = {'a1': [regression_params[0]], 'b1': [regression_params[1]], 'c1': [regression_params[2]], 
           'a2': [regression_params[3]], 'b2': [regression_params[4]], 'c2': [regression_params[5]],
           'h':[regression_params[6]]}
save_params = DataFrame.from_dict(dic)
save_params.to_csv(path + r'\regression_params.csv', sep=';', index=False)

time = numpy.arange(0, 180, 0.01)
regression_data = model_func_2_peaks(regression_params, time)

#goodness of fit (R^2):
time_discrete = numpy.arange(0, 180, 10)
regression_points = model_func_2_peaks(regression_params, time_discrete) #values of the fitted function at the given points
mean_y = numpy.mean(y) #the mean value of the "experimental" data
residuals = y - regression_points
r_squared = 1.0 - (numpy.sum(residuals ** 2) / numpy.sum((y - mean_y)**2))

#pyplot.errorbar(x, y, yerr=error_perc*x, capsize=5, marker='x', linestyle='', color='blue')
pyplot.plot(x, y, marker = 'x', linestyle='', color = 'blue')
pyplot.plot(time, regression_data, color='orange')
pyplot.ylabel('Data [AU]')
pyplot.xlabel('Time t [min]')
pyplot.legend(["Theor. data points", "Regression, R^2 = " + str(round(r_squared, 4))])
pyplot.show()

