import sys
import csv
import matplotlib.pyplot as pyplot
import numpy

y_min = 0.0 # The smallest possible y value
#y_max = 2000000 # guess what
margin = 10

def PlotOutput(timeSeriesA, timeSeriesB, timeScale, path, names, title, savepath):
    y_a_max = max(timeSeriesA)
    y_b_max = max(timeSeriesB)
    #Cut timescale for when A and B have different dimensions:
    timeScaleA = [timeScale[i] for i in range(len(timeSeriesA))]
    timeScaleB = [timeScale[i] for i in range(len(timeSeriesB))]
    
    #plot raw Data:
    fig, ax1 = pyplot.subplots()
    ax1.set_xlabel("Time t [min]")
    ax1.tick_params(axis='y', labelcolor='r')
    ax1.set_ylim(y_min, 2 * y_a_max)
    ax1.set_ylabel("Signal Strength [AU]", color='r')
    plotA, = ax1.plot(timeScaleA, timeSeriesA, linestyle='-', marker='.', color='r')
    
    ax2 = ax1.twinx()
    ax2.tick_params(axis='y', labelcolor='b')
    ax2.set_ylim(y_min - y_b_max, y_b_max)
    ax2.set_ylabel("Signal Stength [AU]", color='b')
    plotB, = ax2.plot(timeScaleB, timeSeriesB, linestyle='-', marker='.', color='b')
    fig.tight_layout()
    pyplot.title(title)
    #plot mapping:
    n = 0
    for i in path:
        if n % 2 == 0:
            ax1.plot([timeScaleA[i[0]], timeScaleB[i[1]]],[timeSeriesA[i[0]], timeSeriesB[i[1]]/(y_b_max) * y_a_max + y_a_max], color='g')
        else:
            ax1.plot([timeScaleA[i[0]], timeScaleB[i[1]]],[timeSeriesA[i[0]], timeSeriesB[i[1]]/(y_b_max) * y_a_max + y_a_max], color='y')
        n += 1
    pyplot.legend([plotA, plotB], [names[1], names[2]])

    #save img
    name = title + '_' + names[2]+' vs '+names[1] + ' ALGORITHM'
    name = name.replace(' ', '_')
    pyplot.savefig(savepath + name, bbox_inches='tight')
    
    
    #Plot time vs time diagram
    pyplot.figure()
    for i in range(len(path)):
        if i == len(path) - 1:
            continue
        p = path[i]
        ppo = path[i+1]
        color = ''
        if i % 2 == 0:
            color = 'purple'
        else:
            color = 'orange'
        pyplot.plot([timeScale[p[0]], timeScale[ppo[0]]], [timeScale[p[1]], timeScale[ppo[1]]], color=color)
    
    pyplot.plot([0, timeScale[len(timeScale)-1]], [0, timeScale[len(timeScale)-1]], 'grey', alpha=0.5)
    pyplot.xlabel(names[1]+" Time [min]")
    pyplot.ylabel(names[2]+" Time [min]")

    #save img
    name = title + '_' + names[2]+' vs '+names[1] + ' TIME'
    name = name.replace(' ', '_')
    pyplot.savefig(savepath + name, bbox_inches='tight')
    #Don't show here, so all 3 windows get opened later
    #pyplot.show()
