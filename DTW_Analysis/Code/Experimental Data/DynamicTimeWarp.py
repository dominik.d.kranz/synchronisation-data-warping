import numpy
import math
from pandas import *
import os
import sys
distanceMatrix = []
def DynamicTimeWarp(timeSeriesA, timeSeriesB, timeScale):
    PLOTTING = True #Programm will call GraphOutput.py to plot the output data if True
    FUNCTION = True #Programm will call TimeWarpFunction.py if true
    TRAILING = False #If a Series is shorter than the other, the last value is repeated until they match
    NORMALIZING = False #Normalizes the Y Axis. Highest point becomes 1
    #MATCH_TIMESCALE = True #If SeriesA and/or SeriesB have a different number of entrys, the will be stretched or zoomed to match the time scale

    
    if TRAILING:
        seriesALength = length(timeSeriesA)
        seriesBLength = length(timeSeriesB)
        timeScaleLength = length(timeScale)
        if seriesALength < timeScaleLength:
            trailedA = timeSeriesA
            trailedA.extend(timeSeriesA[len(timeSeriesA)-1] for i in numpy.arange(0, 0.3*seriesALength))
            timeSeriesA = trailedA
        if seriesBLength < timeScaleLength:
            trailedB = timeSeriesB
            trailedB.extend(timeSeriesB[len(timeSeriesB)-1] for i in numpy.arange(0, 0.3*seriesBLength))
            timeSeriesB = trailedB
    #*********NORMALIZING*************
    if NORMALIZING:
        highestA = max(timeSeriesA)
        normalizedA = [i/highestA for i in timeSeriesA]
        timeSeriesA = normalizedA

        highestB = max(timeSeriesB)
        normalizedB = [i/highestB for i in timeSeriesB]
        timeSeriesB = normalizedB
    #*********END NORMALIZING*********

    CalculateDistanceMatrix(timeSeriesA, timeSeriesB)
    path = GetPathing(distanceMatrix)
    #pandas DataFrame object for pretty printing
    print(DataFrame(distanceMatrix))

    print("*********************************")
    print("*********************************")
    print("Pathing:")
    print(path)

    return (path, timeSeriesA, timeSeriesB)



#Distance between two points
def dist(x, y):
    return abs(y-x)




def GetSmallestNeighbour(currentI, currentJ):
    """
    Checks the current position for the smallest neighbour, and returns its position and value in a tupel

    Returns: ([i, j], value)
    """
    
    #edge case (0,0):
    if currentI == 0 and currentJ == 0:
        return ([0,0], 0)

    neighbour1 = [currentI -1, currentJ]
    neighbour2 = [currentI -1, currentJ-1]
    neighbour3 = [currentI, currentJ-1]

    #check if a neighbour is outside the matrix, if yes, set its value to infinity, so it won't go there.
    #this is a tad bit ugly, surely there's a better way.
    if neighbour1[0] < 0 or neighbour1[1] < 0:
        value1 = math.inf
    else:
        value1 = distanceMatrix[neighbour1[0]][neighbour1[1]]
    if neighbour2[0] < 0 or neighbour2[1] < 0:
        value2 = math.inf
    else:
        value2 = distanceMatrix[neighbour2[0]][neighbour2[1]]
    if neighbour3[0] < 0 or neighbour3[1] < 0:
        value3 = math.inf
    else:
        value3 = distanceMatrix[neighbour3[0]][neighbour3[1]]

    minimalValue = min(value1, value2, value3)
    smallestNeighbour = []
    if minimalValue == value1:
        smallestNeighbour = neighbour1
    if minimalValue == value2:
        smallestNeighbour = neighbour2
    if minimalValue == value3:
        smallestNeighbour = neighbour3
    return (smallestNeighbour, minimalValue)

def CalculateDistanceMatrix(A, B):
    """
    Fills in the DistanceMatrix D, where D(i,j) is the distance of A(i) and B(j) plus the minimal value of the 3 previous neighbour cells
    """
    #initialize square matrix with right dimensions
    global distanceMatrix 
    distanceMatrix = [[-1 for n in range(len(B))] for m in range(len(A))]

    for i in range(len(A)):
        for j in range(len(B)):
            #value = dist([timeScale[row],A[row]], [timeScale[column], B[column]]) + GetSmallestPreviousNeighbourValue(row, column, distMat)
            #smallestNeighbour = GetSmallestNeighbour(i, j)
            #smallestNeighbourValue = distanceMatrix[smallestNeighbour[0]][smallestNeighbour[1]]
            distance = dist(A[i], B[j])
            neighbour = GetSmallestNeighbour(i, j)[1]
            value = distance + neighbour
            distanceMatrix[i][j] = value
    return distanceMatrix


    
def GetPathing(distMat):
    """
    Calculates the optimal path through the DistanceMatrix

    Returns the Pathing in a List of indice pairs
    """
    currentElement = [len(distMat)-1, len(distMat[0])-1]
    tempPath = [currentElement]
    while currentElement != [0,0]:
        neighbour1 = [currentElement[0]-1, currentElement[1]]
        neighbour2 = [currentElement[0]-1, currentElement[1]-1]
        neighbour3 = [currentElement[0], currentElement[1]-1]

        nextElement = GetSmallestNeighbour(currentElement[0], currentElement[1])[0]
        tempPath.append(nextElement)
        currentElement = nextElement
    return tempPath




def length(timeSeries):
    for i in timeSeries[:]: #copy of the list, b/c deleting while iterating is unsafe
        if i == numpy.inf:
            timeSeries.remove(i)
    return len(timeSeries)