from pandas import DataFrame
import pandas
from DataProvider import DataProvider
import PeakAnalysis
import DynamicTimeWarp
import GraphOutput
import TimeWarpFunction
import PlotTimeWarp

dp = DataProvider()
dp.LoadExperimentalData()

speciesA = dp.speciesA
speciesB = dp.speciesB
timeScale = dp.timeScale

tsA = list(filter(lambda a: a==a, speciesA.values.tolist()))
tsB = list(filter(lambda a: a==a, speciesB.values.tolist()))
path, tsA, tsB = DynamicTimeWarp.DynamicTimeWarp(tsA, tsB, timeScale)
newScale = TimeWarpFunction.TimeWarpFunction(tsA, tsB, timeScale.values.tolist(), path)
names = ["Time"]
names.append(speciesA.name)
names.append(speciesB.name)
GraphOutput.GraphOutput(tsA, tsB, timeScale.values.tolist(), path, names, 'img')
names = ["Old Time", "New Time"]
names.append(speciesA.name)
names.append(speciesB.name)
PlotTimeWarp.PlotTimeWarp(tsA, tsB, timeScale.values.tolist(), newScale, names, 'img')
