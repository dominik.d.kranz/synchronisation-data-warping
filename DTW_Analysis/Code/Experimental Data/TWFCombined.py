from pandas import DataFrame
import pandas
import numpy
import matplotlib.pyplot as pyplot
from pathlib import Path
import DynamicTimeWarp
import TWFDTW
import PlotOutput

#changeable variables:

#input:
filepath = Path(__file__).parent
params_true = pandas.read_csv(str(filepath) + r'\fit_parameters_true.csv', sep=';').iloc[0].values.tolist()
params_synchro = pandas.read_csv(str(filepath) + r'\fit_parameters_synchro.csv', sep=';').iloc[0].values.tolist()
data_synchro = pandas.read_csv(str(filepath) + r'\synchro_data.csv', sep=';')

#labels for graph:
true_trend_label = 'Nocodazole Regression'
warped_points_label = 'Warped Elutriation Data'
title = 'Clb5'
                #True        #synchro
names = ['', 'Nocodazole', 'Elutriation']

savepath = str(filepath) + '\\' + title + '_warp_' + names[2] + '_to_' + names[1]
#time warp and model funcs:

#relative time warp: (a point that's 10% under the synchro curve, will be 10% under the true curve
def f_y(old_time, new_time, yd, synchro_params, true_params):
    return (1-(synchro_model(synchro_params, old_time) - yd)/synchro_model(synchro_params, old_time))*true_model(true_params, new_time)

def f_x(true_trend, synchro_trend, scale, path):
    return TWFDTW.TimeWarpFunction(true_trend, synchro_trend, scale, path)

def synchro_model(params, t):
    a1, b1, c1, a2, b2, c2, a3, b3, c3, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + a3 * numpy.exp(-0.5*numpy.square((t-b3)/c3)) + h

def true_model(params, t):
    a1, b1, c1, a2, b2, c2, a3, b3, c3, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + a3 * numpy.exp(-0.5*numpy.square((t-b3)/c3)) + h



x = data_synchro.iloc[:, 0]
y = data_synchro.iloc[:, 1]

discrete_time = numpy.arange(0, 250, 10)
synchro_trend_discrete = synchro_model(params_synchro, discrete_time)
true_trend_discrete = true_model(params_true, discrete_time)

#min max normierung:
synchro_trend_discrete_normed = [i / max(synchro_trend_discrete) for i in synchro_trend_discrete]
true_trend_discrete_normed = [i / max(true_trend_discrete) for i in true_trend_discrete]
#synchro_trend_discrete_normed = synchro_trend_discrete
#true_trend_discrete_normed = true_trend_discrete

path, tsA, tsB = DynamicTimeWarp.DynamicTimeWarp(true_trend_discrete_normed, synchro_trend_discrete_normed, x)
warped_x = DataFrame(f_x(true_trend_discrete, synchro_trend_discrete, x, path)).iloc[:,0] #I know this casting with iloc seems redundant, but I get an error if I leave it out
warped_y = f_y(x, warped_x, y, params_synchro, params_true)

PlotOutput.PlotOutput(true_trend_discrete, synchro_trend_discrete, discrete_time, path, names, title, str(filepath) + '\\')
#warped_data = f(x, y, params_synchro, params_true)
pyplot.figure()
pyplot.plot(warped_x, warped_y, color='green', marker='x', linestyle='')

#goodness of fit for true trend <-> warped data for comparisons sake:
#goodness of fit (R^2):
nans = warped_y.index[warped_y.apply(numpy.isnan)]
if len(nans) != 0:
    warped_y = warped_y.iloc[0:nans[0]]
    warped_x = warped_x.iloc[0:nans[0]]
last_time_after_warp = warped_x[len(warped_x)-1]
cont_time = numpy.arange(0, last_time_after_warp, 0.1)
true_trend = true_model(params_true, cont_time)
regression_points = true_model(params_true, warped_x) #values of the 'fitted' function at the given points
mean_warped_data = numpy.mean(warped_y) #the mean value of the experimental data
residuals = warped_y- regression_points
r_squared = 1.0 - (numpy.sum(residuals ** 2) / numpy.sum((warped_y - mean_warped_data)**2))

pyplot.plot(cont_time, true_trend)
#pyplot.plot(cont_time, synchronised_model(params_synchro, cont_time))
pyplot.legend([warped_points_label, true_trend_label +', R^2 = ' + str(round(r_squared, 4))])
pyplot.title(title)
pyplot.xlabel('Time t [min]')
pyplot.ylabel('Signal Strength [AU]')
pyplot.savefig(savepath, bbox_inches='tight')
pyplot.show()

