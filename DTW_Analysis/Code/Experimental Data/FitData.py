from pandas import DataFrame
import pandas
import scipy.optimize
import numpy
import matplotlib.pyplot as pyplot
from pathlib import Path
pyplot.tight_layout() #don't cut off labels

#adjust variables for data:
#Might also want to adjust the bounds inside the main loop!
path = str(Path(__file__).parent)
data = pandas.read_csv(path + r'\input_data.csv', sep=';')
#estimation of peaks (to avoid local extrema):                                 Has two peaks?
params0 = { 'Cln2': ([6940578, 51, 15,    7818380 ,  115, 17,  0, 0, 1,    0],  True),
            'Clb5': ([951992, 56, 17,    1337366,  120, 17, 0, 0, 1,    0], True),
            'Sic1': ([5634141, 11, 21,   1966838,   103, 25,   0, 0, 1,    0], True)
           }
def model_func(params, t):
    a1, b1, c1, a2, b2, c2, a3, b3, c3, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + a3 * numpy.exp(-0.5*numpy.square((t-b3)/c3)) + h

def residue_func(params, t, y):
    return numpy.multiply(numpy.divide(1, numpy.arange(0.1, 0.15, len(t))), model_func(params, t) - y)



x = data.iloc[:, 0]
y_block = data.iloc[:, 1:4]

count = 0     
for col in y_block:
    
    y = y_block[col]
    nans = y.index[y.apply(numpy.isnan)]
    if len(nans) != 0:
        y = y.iloc[0:nans[0]]
    x = numpy.arange(0, (len(y))*10, 10)
    if params0[col][1]: #two peaks:
        bounds = ([0,          0,          0,          0,           0,          0,        0,    0,    1,    0]
                 ,[numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf, 0.01, 0.01, 1.01, numpy.inf,])
    else: #three peaks:
        bounds = ([0,          0,          0,          0,           0,          0,         0,         0,         0,          0]
                 ,[numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf,  numpy.inf, numpy.inf, numpy.inf,  numpy.inf,])
    
    res_lsq = scipy.optimize.least_squares(residue_func, params0[col][0], bounds=bounds, args=(x, y), method='trf')
    print(res_lsq.x)
    pyplot.figure()
    pyplot.plot(x, y, label='Data', linestyle='', marker='x')
    pyplot.title(col)

    params = res_lsq.x
    dic = {'a1': [params[0]], 'b1': [params[1]], 'c1': [params[2]], 
           'a2': [params[3]], 'b2': [params[4]], 'c2': [params[5]], 
           'a3': [params[6]], 'b3': [params[7]], 'c3': [params[8]],
           'h':[params[9]]}
    df = DataFrame.from_dict(dic)
    savepath = str(path) + r"\fit_parameters_" + col
    df.to_csv(index=False, path_or_buf=savepath+'.csv', sep=';')
    
    t_continous = numpy.arange(0, (len(x)-1)*10, 0.01)
    model_continous = model_func(res_lsq.x, t_continous)
    model_discrete = model_func(res_lsq.x, x)
    #goodness of fit (R^2):
    mean_y = numpy.mean(y)
    residuals = y - model_discrete
    r_squared = 1.0 - (numpy.sum(residuals ** 2) / numpy.sum((y - mean_y)**2))

    #save data
    x = DataFrame(x, columns=['Time'])
    y = y.to_frame()
    y.columns  = [col]
    savedata = pandas.concat([x, y], axis=1)
    savedata.to_csv(index=False, path_or_buf=str(path)+ r'\synchro_data_' + col + '.csv', sep=';')
    #plot
    pyplot.plot(t_continous, model_continous, label='Regression, R^2 = ' + str(numpy.round(r_squared, 4)))
    pyplot.xlabel("Time t [min]")
    pyplot.ylabel("Signal Strength S [AU]")


    pyplot.legend()
    pyplot.savefig(savepath + '.png', bbox_inches='tight')
    count += 1
pyplot.show()