from pandas import DataFrame
import pandas
from DataProvider import DataProvider
import PeakAnalysis
import DynamicTimeWarp
import GraphOutput
import TimeWarpFunction
import PlotTimeWarp

savepath = "Graphs\\HydroxyVsAlphaFactor\\"

dp = DataProvider()
dp.LoadExperimentalDataBlock()
#firstPeakA, firstPeakB = PeakAnalysis.PeakAnalysis(dp.GetSpeciesForPeakAdjustment())
#dp.CutDataForPeaks(firstPeakA, firstPeakB)

###tsA = dp.speciesA.iloc[:, 0].values.tolist() #cln2 A
###tsB = dp.speciesB.iloc[:, 0].values.tolist() #cln2 B
###ts = dp.timeScale.values.tolist()
###path = DynamicTimeWarp.DynamicTimeWarp(tsA, tsB, ts)

###newScale = TimeWarpFunction.TimeWarpFunction(tsA, tsB, ts, path)
###GraphOutput.GraphOutput(tsA, tsB, ts, path, ["Time", "Cln2 Hydroxy", "Cln2 Nocodazole"])
###PlotTimeWarp.PlotTimeWarp(tsA, tsB, ts, newScale, ["Old Time", "New Time", "Cln2 Hydroxy", "Cln2 Nocodazole"])

species = pandas.concat([dp.speciesA, dp.speciesB], axis=1)
print(species)
print(species)
ts = dp.timeScale.values.tolist()
for i in range(8):
    for j in range(8)[i+1:]:
        cutA = species.iloc[:, i]
        cutB = species.iloc[:, j]
        tsA_nan = cutA.values.tolist()
        tsB_nan = cutB.values.tolist()
        #remove NaN's: (a != a only for NaN) clever, right? yeah, I stole it from StackOverflow...
        tsA = list(filter(lambda a: a==a, tsA_nan))
        tsB = list(filter(lambda a: a==a, tsB_nan))


        path, tsA, tsB = DynamicTimeWarp.DynamicTimeWarp(tsA, tsB, ts)
        newScale = TimeWarpFunction.TimeWarpFunction(tsA, tsB, ts, path)
        names = ["Time"]
        names.append(cutA.name)
        names.append(cutB.name)
        GraphOutput.GraphOutput(tsA, tsB, ts, path, names, savepath)
        names = ["Old Time", "New Time"]
        names.append(cutA.name)
        names.append(cutB.name)
        PlotTimeWarp.PlotTimeWarp(tsA, tsB, ts, newScale, names, savepath)
        