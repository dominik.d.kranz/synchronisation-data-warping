from pandas import DataFrame
import pandas
import numpy
import matplotlib.pyplot as pyplot
from pathlib import Path

#changeable variables:
path = Path(__file__).parent
savepath = str(path) + r'\warp.png'
params_true = pandas.read_csv(str(path) + r'\fit_parameters_true.csv', sep=';').iloc[0].values.tolist()
params_synchro = pandas.read_csv(str(path) + r'\fit_parameters_synchro.csv', sep=';').iloc[0].values.tolist()
data_points = pandas.read_csv(str(path) + r'\synchro_data.csv', sep=';')

#labels for graph:
true_trend_label = 'Alpha Factor Regression'
warped_points_label = 'Warped Nocodazole Data'
title = 'Cln2'

#time warp and model funcs:
#Absolute time warp:
#def f(xd, yd, synchro_params, true_params):
#    return yd - (synchronised_model(synchro_params, xd) - true_model(true_params, xd))
#relative time warp: (a point that's 10% under the synchro curve, will be 10% under the true curve
def f(xd, yd, synchro_params, true_params):
    return (1-(synchronised_model(synchro_params, xd) - yd)/synchronised_model(synchro_params, xd))*true_model(true_params, xd)

def synchronised_model(params, t):
    a1, b1, c1, a2, b2, c2, a3, b3, c3, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + a3 * numpy.exp(-0.5*numpy.square((t-b3)/c3)) + h

def true_model(params, t):
    a1, b1, c1, a2, b2, c2, a3, b3, c3, h = params
    return a1 * numpy.exp(-0.5*numpy.square((t-b1)/c1)) + a2 * numpy.exp(-0.5*numpy.square((t-b2)/c2)) + a3 * numpy.exp(-0.5*numpy.square((t-b3)/c3)) + h



x = data_points.iloc[:, 0]
y = data_points.iloc[:, 1]
warped_data = f(x, y, params_synchro, params_true)
pyplot.plot(x, warped_data, color='green', marker='x', linestyle='')
#pyplot.plot(x, y, color='red', marker='x', linestyle='')
cont_time = numpy.arange(0, 180, 0.1)
true_trend = true_model(params_true, cont_time)
#goodness of fit for true trend <-> warped data for comparisons sake:
#goodness of fit (R^2):
regression_points = true_model(params_true, x) #values of the 'fitted' function at the given points
mean_warped_data = numpy.mean(warped_data) #the mean value of the experimental data
residuals = warped_data - regression_points
r_squared = 1.0 - (numpy.sum(residuals ** 2) / numpy.sum((warped_data - mean_warped_data)**2))

pyplot.plot(cont_time, true_trend)
#pyplot.plot(cont_time, synchronised_model(params_synchro, cont_time))
pyplot.legend([warped_points_label, true_trend_label +', R^2 = ' + str(round(r_squared, 4))])
pyplot.title(title)
pyplot.xlabel('Time t [min]')
pyplot.ylabel('Data [AU]')
pyplot.savefig(savepath, bbox_inches='tight')
pyplot.show()

