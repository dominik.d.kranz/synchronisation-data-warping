import numpy
import sys
import math
import csv
from pandas import DataFrame


def TimeWarpFunction(timeSeriesA, timeSeriesB, timeScale, path):
    M = calculateM([len(timeSeriesA), len(timeSeriesB)], path)
    t_n = []
    interval_size = timeScale[1] - timeScale[0]
    for i in range(min([len(M), len(M[0])])):
        t_n.append(T(i*interval_size, M, interval_size))
    return(t_n)
def calculateM(dimension, path):
    m = dimension[0]
    n = dimension[1]
    M = [[0 for i in range(n)] for i in range(m)]
    for i in path:
        M[i[0]][i[1]] = 1
    print(DataFrame(M))
    return M

#implementation with a = min()
#def a(t, M, interval_size):
#    index = int(t/ interval_size)
#    for i in range(len(M)):
#        if M[i][index] == 1:
#            return i

#implementation with a = med()
def a(t, M, interval_size):
    index = int(t/interval_size)
    arr = []
    for i in range(len(M)):
        if M[i][index] == 1:
            arr.append(i)
    return numpy.median(arr)

def b(r, M):
    arr = []
    for i in range(len(M[0])):
        if M[r][i] == 1:
            arr.append(i)
    return numpy.median(arr)

def T(t, M, interval_size):
    A = a(t, M, interval_size)
    B = b(int(A), M)
    colSum = sum(M[int(A)], 0)
    if colSum == 1:
        return A*interval_size
    else:
        return A*interval_size-(B*interval_size-t)/colSum


