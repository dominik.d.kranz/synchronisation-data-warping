import sys
import csv
import matplotlib.pyplot as pyplot
from DataProvider import DataProvider

def PlotTimeWarp(timeSeriesA, timeSeriesB, oldScale, newScale, names, savepath):
    #plot raw Data:
    pyplot.figure()
    pyplot.subplot(311)
    #pyplot.title("Comparison between warped time and normal time")
    
    oldScaleA = [oldScale[i] for i in range(len(timeSeriesA))]
    oldScaleB = [oldScale[i] for i in range(len(timeSeriesB))]
    
    plotA, = pyplot.plot(oldScaleA, timeSeriesA, linestyle='-', marker='.', color='r')
    pyplot.legend([plotA], [names[2]])
    axes = pyplot.gca()
    axes.set_xlim([0, max(oldScale)])
    axes.set_xlabel("Time t [min]")
    axes.tick_params(axis='y', labelcolor='r')
    axes.set_ylabel("Data [AU]", color='r')
    
    pyplot.subplot(312)   
    plotB, = pyplot.plot(oldScaleB, timeSeriesB, linestyle='-', marker='.', color='b')
    pyplot.legend([plotB], [names[3]])
    axes = pyplot.gca()
    axes.set_xlim([0, max(oldScale)])
    axes.set_xlabel("Time t [min]")
    axes.tick_params(axis='y', labelcolor='b')
    axes.set_ylabel("Data [AU]", color='b')

    pyplot.subplot(313)
    plotBNewTime, = pyplot.plot(newScale, timeSeriesB, linestyle='-', marker='.', color='m')
    pyplot.legend([plotBNewTime], [names[3] + " warped"])
    axes = pyplot.gca()
    axes.set_xlim([0, max(oldScale)])
    axes.set_xlabel("Time t [min]")
    axes.tick_params(axis='y', labelcolor='m')
    axes.set_ylabel("Data [AU]", color='m')

    name = names[2]+' vs '+names[3] + ' SUMMARY'
    name = name.replace(' ', '_')
    pyplot.tight_layout()
    pyplot.savefig(savepath + name)
    pyplot.show()