from pandas import DataFrame
import numpy
import pandas
#from DataProvider import DataProvider
import PeakAnalysis
import DynamicTimeWarp
import PlotOutput
import TimeWarpFunction
import PlotTimeWarp

noise_sd = 20.0

timeScale = numpy.arange(0, 180, 5)
speciesA = 70*numpy.exp(-numpy.square(timeScale-35)/(2*numpy.square(8))) + 100 * numpy.exp(-numpy.square(timeScale-100)/(2*numpy.square(8.5)))
speciesB = 40*numpy.exp(-numpy.square(timeScale-50)/(2*numpy.square(6))) + 120 * numpy.exp(-numpy.square(timeScale-125)/(2*numpy.square(6)))

#noise:
speciesA = [i + numpy.random.normal(0, noise_sd) for i in speciesA]
speciesB = [i + numpy.random.normal(0, noise_sd) for i in speciesB]

path, tsA, tsB = DynamicTimeWarp.DynamicTimeWarp(speciesA, speciesB, timeScale)
newScale = TimeWarpFunction.TimeWarpFunction(tsA, tsB, timeScale, path)
names = ["Time"]
names.append("Series A")
names.append("Series B")
PlotOutput.PlotOutput(tsA, tsB, timeScale, path, names, 'img')
names = ["Old Time", "New Time"]
names.append("Series A")
names.append("Series B")
PlotTimeWarp.PlotTimeWarp(tsA, tsB, timeScale, newScale, names, 'img')


