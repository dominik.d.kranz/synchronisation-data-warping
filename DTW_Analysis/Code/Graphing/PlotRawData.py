from pandas import DataFrame
import pandas
from matplotlib import pyplot as pyplot
import numpy
from DataProvider import DataProvider



data = pandas.read_csv("Data\\synchro_data.csv", sep=';')
time = data.iloc[:,0]
cln2 = data.iloc[:,1]
clb5 = data.iloc[:,2]
sic1 = data.iloc[:,3]
gluc = data.iloc[:,4]

error_perc = numpy.linspace(0.01, 0.10, 19)
error = cln2 * error_perc

pyplot.errorbar(time, cln2, yerr=cln2 * error_perc, fmt='-o', capsize=5, elinewidth=0.5)
pyplot.show()